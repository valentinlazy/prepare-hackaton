# Default Apache virtualhost template
{% for item in apache.hosts %}
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot {{ item.to }}
    ServerName {{ item.map }}

    <Directory {{ item.to }}>
        AllowOverride All
        Options -Indexes FollowSymLinks
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>

{% endfor %}