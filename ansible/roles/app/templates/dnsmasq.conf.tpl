domain-needed
bogus-priv

# define local domain part
# this will allow you to have domains with .dev extension,
# so make sure all your development apps are using .dev as extension,
# e.g. somedomain.dev, myapp.dev
local=/lh/
domain=lh

# listen on both local machine and private network
listen-address=127.0.0.1
listen-address={{ vagrant_local.vm.ip }}

# read domain mapping from this file as well as /etc/hosts
addn-hosts=/home/vagrant/hosts
expand-hosts