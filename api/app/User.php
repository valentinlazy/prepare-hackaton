<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
        'first_name', 'last_name',
        'sex', 'logo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
        'created_at', 'updated_at'
    ];

    protected static function generateUniqueToken()
    {
        do {
            $token = uniqid('API_TOKEN');
        } while (self::where('api_token', $token)->exists());

        return $token;
    }

    public function places()
    {
        return $this->hasMany('App\Place', 'user_id');
    }

    public function achivements($needAll = false)
    {
        $achives = UserAchive::where('user_id', $this->id)->get();

        $achives = array_map(function ($item) {
            return $item['achive_id'];
        }, $achives->toArray());

        if ($needAll) {
            $allAchivs = Achiv::all()->toArray();

            foreach ($allAchivs as &$achiv) {
                if (in_array($achiv['id'], $achives)) {
                    $achiv['done'] = true;
                } else {
                    $achiv['done'] = false;
                }
            }

            return $allAchivs;
        }

        if ($achives) {
            return Achiv::where('id', $achives)->get()->toArray();
        }

        return [];
    }
}
