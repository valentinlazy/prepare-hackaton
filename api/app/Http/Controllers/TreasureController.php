<?php

namespace App\Http\Controllers;

use Auth;
use App\Treasure;
use App\Place;
use App\UserAchive;
use Illuminate\Http\Request;

use App\Http\Requests;

class TreasureController extends Controller
{
    public function store($placeID, Request $request) {
        $user = Auth::guard('api')->user();

        Place::findOrFail($placeID);

        $params = $request->all();
        $params['user_id'] = $user->id;
        $params['place_id'] = $placeID;
        $params['active'] = 1;

        if (empty($params['code'])) {
            $params['code'] = Treasure::generateUniqueToken();
        }

        $treasure = Treasure::create($params);
        $treasure = $treasure->toArray();
        $treasure['active'] = 1;

        return response()->json([
            'success' => true,
            'data' => $treasure
        ]);
    }

    public function remove($placeID, $treasureID) {
        $user = Auth::guard('api')->user();

        Treasure::where('place_id', $placeID)->where('id', $treasureID)->where('user_id', $user->id)->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function code() {
        return response()->json([
            'success' => true,
            'data' => ['code' => Treasure::generateUniqueToken()]
        ]);
    }

    public function found($placeID, $treasureID, Request $request) {
        $code = $request->get('confirmation_code');

        $user = Auth::guard('api')->user();

        $treasure = Treasure::where('place_id', $placeID)->where('id', $treasureID);

        if ($code) {
            $treasure->where('confirmation_code', $code)->first();

            if (!$treasure->exists()) {
                return response()->json([
                    'success' => false,
                    'error' => 'Неправильный код подтверждения!'
                ]);
            } else {
                $treasure->update([
                    'active' => 2,
                    'finder_id' => $user->id
                ]);

                UserAchive::create([
                    'user_id' => $user->id,
                    'achive_id' => UserAchive::ACHIVE_FIRST_TREASURE_FOUND
                ]);
            }
        } elseif ($image = $request->get('confirmation_image')) {
            $treasure->update(['confirmation_image' => $image, 'finder_id' => $user->id]);
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function approveImage($placeID, $treasureID) {
        $user = Auth::guard('api')->user();

        $treasure = Treasure::where('place_id', $placeID)->where('id', $treasureID)->where('user_id', $user->id)->firstOrFail();

        $treasure->update(['active', 2]);

        UserAchive::create([
            'user_id' => $treasure->finder_id,
            'achive_id' => UserAchive::ACHIVE_FIRST_TREASURE_FOUND
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function declineImage($placeID, $treasureID) {
        $user = Auth::guard('api')->user();

        $treasure = Treasure::where('place_id', $placeID)->where('id', $treasureID)->where('user_id', $user->id)->firstOrFail();

        $treasure->update(['confirmation_image', '']);

        return response()->json([
            'success' => true
        ]);
    }
}
