<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Validator;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers;

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => 'invalid request']);
        }

        if (!($user = User::where('email', $credentials['email'])->exists())) {
            $user = User::create(['email' => $credentials['email'], 'password' => Hash::make($credentials['password'])]);

            Auth::guard('web')->login($user);

            $response = Auth::guard('web')->user()->toArray();
            $response['isNew'] = true;
            $response['api_token'] = Auth::guard('web')->user()->api_token;

            return response()->json(['success' => true, 'data' => $response]);
        }

        if (Auth::guard('web')->attempt(['email' => $credentials['email'], 'password' => $credentials['password']])) {
            $response = Auth::guard('web')->user()->toArray();
            $response['api_token'] = Auth::guard('web')->user()->api_token;

            return response()->json(['success' => true, 'data' => $response]);
        }

        return response()->json(['success' => false, 'error' => 'invalid credentials']);
    }
}