<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('places')->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }

    public function getAchivements()
    {
        $user = Auth::guard('api')->user();

        $achivs = $user->achivements(true);

        return response()->json([
            'success' => true,
            'data' => $achivs
        ]);
    }

    public function updateCurrentUser(UpdateUserRequest $request)
    {
        $user = Auth::guard('api')->user();

        $user->update($request->all());

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }
}
