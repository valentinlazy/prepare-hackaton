<?php

namespace App\Http\Controllers;

use App\Treasure;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePlaceRequest;
use App\Place;
use App\LikePlace;
use App\FavoritePlace;
use App\Http\Requests;
use Auth;
use Illuminate\Http\Response;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('tag');

        $places = Place::with(['author', 'likes', 'dislikes', 'treasures']);

        if ($filter) {
            $filter = explode(',', $filter);
            $utf8filters = array_map('utf8_decode', $filter);
            $filter = $filter + $utf8filters;

            $places->where(function ($query) use ($filter) {
                $query->where('tags', 'LIKE', "%{$filter[0]}%");

                if (count($filter) > 1) {
                    $query->where('tags', 'LIKE', "%{$filter[0]}%");

                    for ($i = 1; $i < count($filter); $i++) {
                        $query->orWhere('tags', 'LIKE', "%{$filter[$i]}%");
                    }
                }
            });
        }

        $data = $places->get();

        $with = $without = [];

        $currentLocation = $request->get('location');
        if ($currentLocation) {
            $currentLocation = json_decode($currentLocation, true);
        }

        foreach ($data as $place) {
            $found = false;

            $tags = json_decode($place->tags, true);

            if (!$tags) {
                $tags = [];
            }

            $place->tags = $tags;
            $place->coordinates = json_decode($place->coordinates);
            if ($currentLocation) {
                $place->calculateDistance($place->coordinates[0], $place->coordinates[1], $currentLocation[0], $currentLocation[1]);
            }

            foreach ($place->treasures as $treasure) {
                if ($treasure->active == 1) {
                    $with[] = $place;

                    $found = true;

                    break;
                }
            }

            if (!$found) {
                $without[] = $place;
            }
        }

        return response()->json([
            'data' => [
                'with' => $with,
                'without' => $without
            ],
            'success' => true
        ]);
    }

    public function indexList(Request $request) {
        $places = Place::with(['author', 'likes', 'dislikes', 'treasures'])->get();

        $currentLocation = $request->get('location');
        if ($currentLocation) {
            $currentLocation = json_decode($currentLocation, true);
        }

        foreach ($places as &$place) {
            $tags = json_decode($place->tags, true);

            if (!$tags) {
                $tags = [];
            }

            $place->tags = $tags;
            $place->coordinates = json_decode($place->coordinates);

            if ($currentLocation) {
                $place->calculateDistance($place->coordinates[0], $place->coordinates[1], $currentLocation[0], $currentLocation[1]);
            }
        }

        $places = $places->toArray();

        usort($places, function ($a, $b) {
            return $a['distance'] > $b['distance'];
        });

        return response()->json([
            'data' => $places,
            'success' => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(CreatePlaceRequest $request)
    {
        $user = Auth::guard('api')->user();

        $params = $request->all();
        $params['tags'] = json_encode($params['tags'], true);
        $params['coordinates'] = json_encode($params['coordinates'], true);
        $params['user_id'] = $user->id;

        $place = Place::create($params);

        if (!empty($params['treasure']) && !empty($params['treasure']['tip']) && !empty($params['treasure']['logo'])) {
            Treasure::create($params['treasure'] + ['user_id' => $user->id, 'place_id' => $place->id, 'active' => 1]);
        }

        $place->load('treasures');


        return response()->json(['success' => true, 'data' => $place]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $model = Place::with(['author', 'treasures', 'likes', 'dislikes'])->findOrFail($id);
        $model->tags = json_decode($model->tags, true);

        return response()->json([
            'success' => true,
            'data' => $model
        ]);
    }

    public function like($id)
    {
        Place::findOrFail($id);

        $user = Auth::guard('api')->user();

        LikePlace::where('user_id', $user->id)->where('place_id', $id)->where('value', Place::DISLIKE)->delete();

        LikePlace::firstOrCreate([
            'user_id' => $user->id,
            'place_id' => $id,
            'value' => Place::LIKE
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function dislike($id)
    {
        Place::findOrFail($id);

        $user = Auth::guard('api')->user();

        LikePlace::where('user_id', $user->id)->where('place_id', $id)->where('value', Place::LIKE)->delete();

        LikePlace::firstOrCreate([
            'user_id' => $user->id,
            'place_id' => $id,
            'value' => Place::DISLIKE
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function favorite($id)
    {
        Place::findOrFail($id);

        $user = Auth::guard('api')->user();

        FavoritePlace::firstOrCreate([
            'user_id' => $user->id,
            'place_id' => $id
        ]);

        return response()->json([
            'success' => true
        ]);
    }
}
