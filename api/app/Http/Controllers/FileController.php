<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use URL;

class FileController extends Controller
{
    public function upload(Request $request) {
        if (!$request->hasFile('data') || !$request->file('data')->isValid()) {
            return response()->json([
                'success' => false,
                'error' => 'File does not present in the request'
            ]);
        }

        $destination = public_path() . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;

        $filename = time() . '.' . $request->file('data')->getClientOriginalExtension();

        $request->file('data')->move($destination, $filename);

        $url = URL::to('/') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $filename;

        return response()->json([
            'success' => true,
            'data' => ['url' => $url]
        ]);
    }
}