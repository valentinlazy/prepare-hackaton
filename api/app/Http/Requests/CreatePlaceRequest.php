<?php

namespace App\Http\Requests;

class CreatePlaceRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'logo' => 'required',
            'tags' => 'required',
            'coordinates' => 'required'
        ];
    }
}
