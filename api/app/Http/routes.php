<?php
Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::any('/account', 'UserController@updateCurrentUser');

        Route::get('/achivements', 'UserController@getAchivements');

        Route::post('place/{id}/like', 'PlaceController@like');
        Route::post('place/{id}/dislike', 'PlaceController@dislike');
        Route::post('place/{id}/favorite', 'PlaceController@favorite');

        Route::get('place/list', 'PlaceController@indexList');
        Route::resource('place', 'PlaceController', ['except' => ['destroy', 'update']]);

        Route::get('treasure/code', 'TreasureController@code');

        Route::post('place/{id}/treasure', 'TreasureController@store');
        Route::delete('place/{id}/treasure', 'TreasureController@remove');
        Route::post('place/{id}/treasure/{treasure_id}/found', 'TreasureController@found');
        Route::post('place/{id}/treasure/{treasure_id}/approve', 'TreasureController@approveImage');
        Route::post('place/{id}/treasure/{treasure_id}/decline', 'TreasureController@declineImage');
    });

    Route::any('/login', 'AuthController@authenticate');
    Route::post('/upload', 'FileController@upload');
    Route::resource('user', 'UserController', ['only' => ['show']]);
});

