<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePlace extends Model {
    public $timestamps = false;

    protected $table = 'likes_places';

    protected $fillable = ['user_id', 'place_id', 'value'];

    protected $hidden = ['id', 'value'];
}