<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAchive extends Model {
    const ACHIVE_FIRST_PLACE_CREATED = 1;
    const ACHIVE_FIVE_PLACE_CREATED = 2;
    const ACHIVE_FIRST_TREASURE_CREATED = 3;
    const ACHIVE_FIVE_TREASURE_CREATED = 4;
    const ACHIVE_FIRST_TREASURE_FOUND = 5;
    const ACHIVE_FIVE_TREASURE_FOUND = 6;

    public $timestamps = false;

    protected $fillable = ['user_id', 'achive_id'];

    protected $table = 'users_achivs';
}