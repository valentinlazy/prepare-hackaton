<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoritePlace extends Model {
    public $timestamps = false;

    protected $table = 'favorites_places';

    protected $fillable = ['user_id', 'place_id'];

    protected $hidden = ['id'];
}