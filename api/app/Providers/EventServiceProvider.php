<?php

namespace App\Providers;

use App\Treasure;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\User;
use App\Place;
use App\UserAchive;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        User::creating(function ($model) {
            $model->api_token = User::generateUniqueToken();

            return true;
        });

        Place::created(function ($model) {
            $placeCount = Place::where('user_id', $model->user_id)->count();

            if ($placeCount == 1) {
                UserAchive::create([
                    'user_id' => $model->user_id,
                    'achive_id' => UserAchive::ACHIVE_FIRST_PLACE_CREATED
                ]);
            } elseif ($placeCount == 5) {
                UserAchive::create([
                    'user_id' => $model->user_id,
                    'achive_id' => UserAchive::ACHIVE_FIVE_PLACE_CREATED
                ]);
            }
        });

        Treasure::created(function ($model) {
            $treasureCount = Treasure::where('user_id', $model->user_id)->count();

            if ($treasureCount == 1) {
                UserAchive::create([
                    'user_id' => $model->user_id,
                    'achive_id' => UserAchive::ACHIVE_FIRST_TREASURE_CREATED
                ]);
            } elseif ($treasureCount == 5) {
                UserAchive::create([
                    'user_id' => $model->user_id,
                    'achive_id' => UserAchive::ACHIVE_FIVE_TREASURE_CREATED
                ]);
            }
        });

        parent::boot($events);
    }
}
