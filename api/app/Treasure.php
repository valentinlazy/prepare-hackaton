<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Treasure extends Model {
    protected $fillable = ['place_id', 'user_id', 'logo', 'tip', 'confirmation_code'];

    protected $hidden = ['confirmation_code', 'user_id', 'place_id', 'confirmation_image'];

    protected $appends = ['treasureConfirmationImage'];

    public static function generateUniqueToken()
    {
        do {
            $token = uniqid();
        } while (self::where('confirmation_code', $token)->where('active', '!=', 1)->exists());

        return $token;
    }

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getTreasureConfirmationImageAttribute() {
        $user = Auth::guard('api')->user();

        if ($this->active == 1 && ($user->id == $this->user_id)) {
            return $this->confirmation_image;
        }

        return '';
    }
}