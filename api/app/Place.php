<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    const LIKE = 1;
    const DISLIKE = 2;

    protected $distance = 0;

    protected $fillable = [
        'description', 'logo',
        'tags', 'social_hash', 'coordinates', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    protected $appends = ['isFavorite', 'distance'];

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function likes() {
        return $this->hasMany('App\LikePlace', 'place_id')->where('value', self::LIKE);
    }

    public function dislikes() {
        return $this->hasMany('App\LikePlace', 'place_id')->where('value', self::DISLIKE);
    }

    public function treasures() {
        return $this->hasMany('App\Treasure', 'place_id')->with('author')->where('active', '!=', 0);
    }

    public function getIsFavoriteAttribute() {
        if (Auth::guest()) {
            return false;
        }

        $user = Auth::user();

        return FavoritePlace::where('user_id', $user->id)->where('place_id', $this->id)->exists();
    }

    public function getDistanceAttribute() {
        return $this->distance;
    }

    public function calculateDistance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        $this->distance = round($miles * 1.609344, 2);
    }
}
