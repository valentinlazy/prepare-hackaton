<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PULL</title>
</head>
<body>
<h1>GIT PULL:</h1>
<pre>
<?php
set_time_limit(0);
chdir(__DIR__.'/../../');
passthru('git pull -f');
?>
</pre>
<h1>GRUNT BUILD:</h1>
<pre>
<?php
chdir(__DIR__.'/../../client');
passthru('grunt build:angular --force');
?>
</pre>
</body>
</html>
