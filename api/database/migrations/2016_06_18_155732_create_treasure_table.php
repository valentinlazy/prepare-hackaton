<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreasureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasures', function ($table) {
            $table->increments('id');
            $table->integer('place_id');
            $table->integer('user_id');
            $table->string('logo')->nullable();
            $table->text('tip')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->integer('finder_id')->nullable();
            $table->enum('active', [0, 1, 2])->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('treasures');
    }
}
