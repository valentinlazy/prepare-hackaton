<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Achiv;

class AddAchivements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achivs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('name');
            $table->text('description');
        });

        Schema::create('users_achivs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('achive_id');

            $table->timestamps();
        });

        Achiv::create([
            'id' => 1,
            'name' => 'Открыто первое место!',
            'description' => 'Поздравляем, вам присвоен статус городского сталкера! Так держать :)'
        ]);

        Achiv::create([
            'id' => 2,
            'name' => 'Открыто пять мест!',
            'description' => 'Уоу уоу, полегче, исследователь'
        ]);

        Achiv::create([
            'id' => 3,
            'name' => 'Заложен первый клад!',
            'description' => 'Уууу да вы пират'
        ]);

        Achiv::create([
            'id' => 4,
            'name' => 'Заложен пятый клад!',
            'description' => 'Теперь вы Джек-Воробей, Капитан Джек Воробей!'
        ]);

        Achiv::create([
            'id' => 5,
            'name' => 'Найден клад!',
            'description' => 'Поздравляю, теперь ты кладоискатель'
        ]);

        Achiv::create([
            'id' => 6,
            'name' => 'Найден пятый клад!',
            'description' => 'Нужно больше золота!'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('achivs');
        Schema::drop('users_achivs');
    }
}
