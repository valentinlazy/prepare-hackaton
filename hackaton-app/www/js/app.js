// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var HTTP_ROOT = 'http://api.hackathon.hacktus.ru/v1/';
var api_token = false;


angular.module('starter', [
  'ionic',
  'ionic.service.core',
  'starter.controllers',
  'yaMap',
  'LocalStorageModule',
  'ngCordova',
  'ngTagsInput',
  'monospaced.elastic'
])

.run(function($ionicPlatform, $state, $http, $rootScope, $window, localStorageService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    $rootScope.mapHeight = ($window.innerHeight - 45) + 'px';

    if (localStorageService.get('user')) {
      var user = localStorageService.get('user');
      $rootScope.user = user;
      console.log(user);

      if (!$rootScope.user.logo) {
        $rootScope.user.logo = 'img/default-user.png';
      }

      $http.defaults.headers.common.Authorization = 'Bearer '+user.api_token;
      api_token = user.api_token;

      $state.go('app.home');
    } else {
      $state.go('login');
    }

  });
})

.run(function($rootScope, $state) {
  $rootScope.$on('$stateChangeStart',
    function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name !== 'login') {
        fromState.params = fromParams;
        $state.previous = fromState;
      } else {
        $state.previous.name = 'app.home';
      }
    }
  );
})
.factory('tokenInterceptorService', ['$q', '$location', function ($q, $state) {

  var tokenInterceptor = {};

  var request = function (config) {
    if (config.url.match(new RegExp(HTTP_ROOT))) {
      if (config.url.match(/\?/)) {
        config.url += '&api_token='+api_token;
      } else {
        config.url += '?api_token='+api_token;
      }
    }

    return config;
  };

  tokenInterceptor.request = request;

  return tokenInterceptor;
}])

.config(function(localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('gp_')
    .setStorageType('localStorage')
    .setNotify(false, false);
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('tokenInterceptorService');
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('profile', {
      url: '/profile',
      templateUrl: 'templates/profile.html',
      controller: 'ProfileCtrl'
    })

    .state('app.listPlaces', {
      url: '/listPlaces',
      views: {
        'menuContent': {
          templateUrl: 'templates/list-places.html',
          controller: 'ListPlacesCtrl'
        }
      }
    })

    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'MainCtrl'
        }
      }
    })

    .state('app.achivements', {
      url: '/achivements',
      views: {
        menuContent: {
          templateUrl: 'templates/achivements.html',
          controller: 'AchivCtrl'
        }
      }
    })

    .state('app.place', {
      url: '/place/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/place.html',
          controller: 'PlaceCtrl'
        }
      }
    })

    .state('app.search', {
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html',
          controller: 'SearchCtrl'
        }
      }
    })

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/login');
})
;
