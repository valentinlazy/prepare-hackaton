angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope) {

})

.controller('LoginCtrl', function ($http, $state, $scope, $ionicModal, localStorageService, $window, $rootScope) {

  $scope.loginData = {};

  $scope.mapHeight = ($window.innerHeight) + 'px';

  $scope.clearLoginData = function() {
    $scope.loginData = {};
  };

  $scope.doLogin = function() {
    $http({
      method: 'post',
      url: HTTP_ROOT+'login',
      data: $scope.loginData
    }).then(function(response) {
      response = response.data;

      if (response.success) {
        localStorageService.set('user', response.data);
        $rootScope.user = response.data;
        api_token = response.data.api_token;

        if (response.data.isNew) {
          $state.go('profile')
        } else {
          $state.go('app.home')
        }

      }
    });
  };

})

  .controller('MainCtrl', function (
    $scope,
    $rootScope,
    yaMapSettings,
    $window,
    $ionicModal,
    $cordovaCamera,
    $cordovaFileTransfer,
    $ionicActionSheet,
    $http,
    $timeout,
    $ionicLoading
  ) {
    $ionicLoading.show({template: 'Загрузка ...'});

    $scope.beforeInit = function() {
      navigator.geolocation.getCurrentPosition(function (loc) {
        $scope.currentLoc = {
          geometry:{
            type:'Point',
            coordinates: [loc.coords.longitude, loc.coords.latitude]
          },
          properties:{
            balloonContent:'Вы здесь'
          }
        };
        $scope.geoObjects.push($scope.currentLoc);
        $scope.center = $scope.currentLoc.geometry.coordinates;
        $scope.$digest();
      });
    };
    $scope.geoObjects=[];

    $ionicModal.fromTemplateUrl('templates/add-place.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.placeFormData = {
      showTreasure: false
    };

    $scope.placeData = {
      logo: '',
      coordinates: '',
      description: '',
      tags: [],
      treasure: {
        logo: '',
        confirmation_code: '',
        tip: ''
      }
    };

    $scope.addPlace = function () {
      $scope.modal.show();
    };

    $scope.closeAddPlace = function () {

      $scope.modal.hide().then(function() {
        $scope.placeFormData.showTreasure = false;

        $scope.placeData = {
          logo: '',
          coordinates: '',
          description: '',
          tags: [],
          treasure: {
            logo: '',
            confirmation_code: '',
            tip: ''
          }
        };

      });

    };

    $scope.addTreasure = function() {
      $scope.placeFormData.showTreasure = true;
    };

    $scope.cancelTreasure = function() {

      $scope.placeFormData.showTreasure = false;

      $scope.placeData = {
        treasure: {
          logo: '',
          confirmation_code: '',
          tip: ''
        }
      };

    };

    $scope.doAdd = function () {
      navigator.geolocation.getCurrentPosition(function (loc) {
        $scope.placeData.coordinates = [loc.coords.longitude, loc.coords.latitude];
        $scope.placeData.tags = $scope.placeData.tags.map(function (item) {
          return item.text;
        });
        $http({
          method: 'post',
          url: HTTP_ROOT+'place',
          data: $scope.placeData
        }).then(function (response) {
          if (response.data.success) {
            $scope.getPlaces()
              .then(function(places) {
                $scope.closeAddPlace();
              });
          }
        });
      });
    };

    $scope.addPictureAction = function (model, prop) {

      var types = [
        Camera.PictureSourceType.CAMERA,
        Camera.PictureSourceType.PHOTOLIBRARY
      ];
      var hideSheet = $ionicActionSheet.show({
        buttons: [
          { text: 'Camera' },
          { text: 'Gallery' }
        ],
        titleText: 'Choose picture',
        cancelText: 'Cancel',
        cancel: function() {
          hideSheet();
        },
        buttonClicked: function(index) {
          $ionicLoading.show({template: 'Загрузка ...'});
          $scope.takePicture(types[index], model, prop);
          hideSheet();
        }
      });
    };

    $scope.takePicture = function (sourceType, model, prop) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {

        var url = HTTP_ROOT+"upload";
        //target path may be local or url
        var targetPath = imageData;
        $scope.imageData = imageData;
        var filename = targetPath.split("/").pop();
        var options = {
          fileKey: "data",
          fileName: filename,
          chunkedMode: false,
          mimeType: "image/jpg"
        };
        $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
          var response = JSON.parse(result.response);
          model[prop] = response.data.url;
          console.log("SUCCESS: " + JSON.stringify(response));
          $ionicLoading.hide();
        }, function(err) {
          console.log("ERROR: " + JSON.stringify(err));
          alert(JSON.stringify(err));
          $ionicLoading.hide();
        }, function (progress) {
          // constant progress updates
          $timeout(function () {
            $scope.downloadProgress = (progress.loaded / progress.total) * 100;
          })
        });

      }, function(err) {
        // error
        console.log(err);
        $ionicLoading.hide();
      });
    };

    $scope.doSearchPlaces = function(e) {
      $scope.getPlaces(e.originalEvent.request);

      // angular.element(document.querySelector('.ymaps-2-1-41-search_layout_panel'))
      //   .removeClass('ymaps-2-1-41-search_layout_panel_show')
      //   .addClass('ymaps-2-1-41-search_layout_panel_hide');
    };

    $scope.doClearSearch = function(e) {
      $scope.getPlaces();
    };

    $scope.doAfterSearchInit = function(target) {
      //console.log(target)
    };

    $scope.places = {with:[], without:[]};

    /**
     * @param array tags
     */
    $scope.getPlaces = function(tags) {
      tags = tags ? tags.replace(/\s/g, ',') : '';

      return $http({
        method: 'get',
        url: HTTP_ROOT+'place',
        params: {tag: tags}
      }).then(function(response) {
        response = response.data;

        angular.forEach(response.data, function(places, placesCategory) {
          $scope.places[placesCategory] = mapPlaces(places);
        });

        $ionicLoading.hide();

        return response;
      });
    };

    function mapPlaces(places) {

      places = places.map(function(place) {
        return {
          geometry:{
            type: 'Point',
            coordinates: place.coordinates
          },

          properties: {
            id: place.id,
            description: place.description.length > 30 ? place.description.slice(0, 30).trim() + '...' : place.description
          }
        }
      });

      return places;
    }

    //init
    $scope.getPlaces();
  })

.controller('ProfileCtrl', function (
  $http,
  $state,
  $scope,
  localStorageService,
  $cordovaCamera,
  $cordovaFileTransfer,
  $ionicActionSheet,
  $timeout,
  $rootScope,
  $ionicLoading
) {

  $scope.profileData = localStorageService.get('user');

  if (!$scope.profileData.logo) {
    $scope.profileData.logo = 'img/default-user.png';
  }

  $scope.doUpdate = function() {
    $http({
      method: 'post',
      url: HTTP_ROOT+'account',
      data: $scope.profileData
    }).then(function(response) {
      response = response.data;

      if (response.success) {
        localStorageService.set('user', $scope.profileData);
        $rootScope.user = response.data;
        api_token = $scope.profileData.api_token;
        $state.go('app.home');
      }

    });
  };

  $scope.goBack = function() {
    $state.go($state.previous.name);
  };

  $scope.doLogout = function() {
    localStorageService.remove('user');
    $state.go('login')
  };

  //$scope.imageData = 'img/camera.jpg';


  $scope.addPictureAction = function () {
    var types = [
      Camera.PictureSourceType.CAMERA,
      Camera.PictureSourceType.PHOTOLIBRARY
    ];
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Camera' },
        { text: 'Gallery' }
      ],
      titleText: 'Choose picture',
      cancelText: 'Cancel',
      cancel: function() {
        hideSheet();
      },
      buttonClicked: function(index) {
        $scope.takePicture(types[index]);
        hideSheet();
      }
    });
  };

  $scope.takePicture = function (sourceType) {
    $ionicLoading.show();
    var options = {
      quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {

      var url = HTTP_ROOT+"upload";
      //target path may be local or url
      var targetPath = imageData;
      //$scope.imageData = imageData;
      var filename = targetPath.split("/").pop();
      var options = {
        fileKey: "data",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpg"
      };
      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
        var response = JSON.parse(result.response);
        $scope.profileData.logo = response.data.url;
        //$scope.imageData = response.data.url;
        console.log("SUCCESS: " + JSON.stringify(response));
        $ionicLoading.hide();
      }, function(err) {
        console.log("ERROR: " + JSON.stringify(err));
        alert(JSON.stringify(err));
        $ionicLoading.hide();
      }, function (progress) {
        // constant progress updates
        $timeout(function () {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
        })
      });

    }, function(err) {
      // error
      console.log(err);
      $ionicLoading.hide();
    });
  };

})

.controller('PlaceCtrl', function (
  $state,
  $scope,
  $http,
  $ionicModal,
  $cordovaCamera,
  $cordovaFileTransfer,
  $ionicActionSheet,
  $ionicLoading,
  $timeout,
  $rootScope
) {
  var stateParams = $state.params,
      placeId = stateParams.id;

  $scope.place = {};
  $scope.placeFormData = {
    showTreasure: false
  };

  $http({
    method: 'get',
    url: HTTP_ROOT+'place/' + placeId
  }).then(function(response) {
    response = response.data;

    $scope.placeData = response.data;
    if (!$scope.placeData.treasures) {
      $scope.placeData.treasures = [];
    }
    $scope.placeData.treasures.map(function (item) {
      if (item.active == 1) {
        $scope.hasTreasure = true;
        $scope.placeData.treasure = item;
      }
      return item;
    });
    if (!$scope.placeData.treasure) {
      $scope.placeData.treasure = {};
    }
  });

  $scope.doOpen = function () {
    $http({
      method: 'post',
      url: HTTP_ROOT+'place/'+$scope.placeData.id+'/treasure/'+$scope.placeData.treasure.id+'/found',
      data: $scope.placeData.treasure
    }).then(function (response) {
      $scope.modal.hide();
    });
  };

  $scope.saveTreasure = function () {
    $http({
      method: 'post',
      url: HTTP_ROOT+'place/'+$scope.placeData.id+'/treasure/',
      data: $scope.placeData.treasure
    }).then(function (response) {
      response = response.data;
      $scope.hasTreasure = true;
      $scope.placeFormData.showTreasure = false;
      $scope.placeData.treasures.push(response.data);
    });
  };
  $scope.addTreasure = function() {
    $scope.placeFormData.showTreasure = true;
  };

  $scope.cancelTreasure = function() {
    $scope.placeFormData.showTreasure = false;
  };

  $scope.accept = function (tr) {
    $ionicLoading.show();
    $http({
      method: 'post',
      url: HTTP_ROOT+'place/'+$scope.placeData.id+'/treasure/'+$scope.placeData.treasure.id+'/approve',
      data: $scope.placeData.treasure
    }).then(function (response) {
      response = response.data;
      tr.treasureConfirmationImage = '';
      tr.active = 2;
      $ionicLoading.hide();
    });
  };
  $scope.decline = function (tr) {
    $ionicLoading.show();
    $http({
      method: 'post',
      url: HTTP_ROOT+'place/'+$scope.placeData.id+'/treasure/'+$scope.placeData.treasure.id+'/decline',
      data: $scope.placeData.treasure
    }).then(function (response) {
      response = response.data;
      tr.treasureConfirmationImage = '';
      $ionicLoading.hide();
    });
  };

  $ionicModal.fromTemplateUrl('templates/open-treasure.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openTreasure = function (model) {
    if (model.active && model.author.id != $rootScope.user.id) {
      $scope.modal.show();
    }
  };

  $scope.hasTreasure = false;

  $scope.addPictureAction = function (model, prop) {

    var types = [
      Camera.PictureSourceType.CAMERA,
      Camera.PictureSourceType.PHOTOLIBRARY
    ];
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Camera' },
        { text: 'Gallery' }
      ],
      titleText: 'Choose picture',
      cancelText: 'Cancel',
      cancel: function() {
        hideSheet();
      },
      buttonClicked: function(index) {
        $ionicLoading.show({template: 'Загрузка ...'});
        $scope.takePicture(types[index], model, prop);
        hideSheet();
      }
    });
  };

  $scope.takePicture = function (sourceType, model, prop) {
    var options = {
      quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {

      var url = HTTP_ROOT+"upload";
      //target path may be local or url
      var targetPath = imageData;
      $scope.imageData = imageData;
      var filename = targetPath.split("/").pop();
      var options = {
        fileKey: "data",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpg"
      };
      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
        var response = JSON.parse(result.response);
        model[prop] = response.data.url;
        console.log("SUCCESS: " + JSON.stringify(response));
        $ionicLoading.hide();
      }, function(err) {
        console.log("ERROR: " + JSON.stringify(err));
        alert(JSON.stringify(err));
        $ionicLoading.hide();
      }, function (progress) {
        // constant progress updates
        $timeout(function () {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
        })
      });

    }, function(err) {
      // error
      console.log(err);
      $ionicLoading.hide();
    });
  };
})

.controller('AchivCtrl', function ($scope, $http) {
  $http({
    method: 'get',
    url: HTTP_ROOT+'achivements'
  }).then(function(response) {
    response = response.data;

    $scope.achivements = response.data;
  });
})

.controller('ListPlacesCtrl', function ($http, $scope) {

  $scope.places = [];

  navigator.geolocation.getCurrentPosition(function (loc) {

    $http({
      method: 'get',
      url: HTTP_ROOT+'place/list',
      params: {location: JSON.stringify([loc.coords.longitude, loc.coords.latitude])}
    }).then(function(response) {
      response = response.data;

      $scope.places = response.data;
    });

  });


})

.controller('SearchCtrl', function ($scope) {

})

.controller('AboutCtrl', function ($scope) {

});
