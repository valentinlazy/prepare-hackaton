#If your Vagrant version is lower than 1.5, you can still use this provisioning
#by commenting or removing the line below and providing the config.vm.box_url parameter,
#if it's not already defined in this Vagrantfile. Keep in mind that you won't be able
#to use the Vagrant Cloud and other newer Vagrant features.
Vagrant.require_version ">= 1.5"

# Check to determine whether we're on a windows or linux/os-x host,
# later on we use this to launch ansible in the supported way
# source: https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby
def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end

path = "#{File.dirname(__FILE__)}"

require 'yaml'

Vagrant.configure("2") do |config|

    config.vm.provider :virtualbox do |v|
        v.name = "hackaton"
        v.customize [
            "modifyvm", :id,
            "--name", "hackaton",
            "--memory", 2048,
            "--natdnshostresolver1", "on",
            "--cpus", 1,
        ]
    end

    config.vm.box = "ubuntu/trusty64"
    
    config.vm.network :private_network, ip: "192.168.33.33"
    config.ssh.forward_agent = true

    # If ansible is in your path it will provision from your HOST machine
    # If ansible is not found in the path it will be instaled in the VM and provisioned from there
    if which('ansible-playbook')
        config.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/playbook.yml"
            ansible.inventory_path = "ansible/inventories/dev"
            ansible.limit = 'all'
        end
    else
        config.vm.provision :shell, path: "ansible/windows.sh", args: ["hackaton"]
    end

    config.vm.synced_folder "./", "/vagrant"

    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
    config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
        if hostname = (vm.ssh_info && vm.ssh_info[:host])
          `vagrant ssh -c "hostname -I"`.split()[1]
        end
    end

    preferences = YAML::load(File.read(path + '/ansible/vars/all.yml'))
    # remove existing hosts file if any
    config.vm.define 'hackaton' do |node|
        node.vm.hostname = 'hackaton.lh'
        node.hostmanager.aliases = []
        preferences["apache"]["hosts"].each do |site|
            node.hostmanager.aliases.push(site['map'])
        end
    end

    config.vm.provision "shell" do |shell|
        shell.inline = "rm -f $1"
        shell.args = ["/home/vagrant/hosts"]
    end
    preferences["apache"]["hosts"].each do |site|
        config.vm.provision "shell" do |shell|
            # populate the domain mapping file from Homestead sites configuration
            # the dot after $2 is important
            shell.inline = "echo $1  $2. >> /home/vagrant/hosts"
            shell.args = [preferences["vagrant_local"]["vm"]["ip"], site["map"]]
        end
    end
end
