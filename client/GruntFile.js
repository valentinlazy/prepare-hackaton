module.exports = function(grunt) {
    var gtx = require('gruntfile-gtx').wrap(grunt);

    gtx.loadAuto();

    var gruntConfig = require('./grunt');
    gruntConfig.package = require('./package.json');

    gtx.config(gruntConfig);

    // build Angular
    gtx.alias('build:angular', [
        'clean:angular',
        'copy',
        'recess:style',
        'concat:general',
        'uglify:general'
    ]);

    gtx.finalise();
}
