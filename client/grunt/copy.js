module.exports = {
    bower_components: {
        files: [
          {expand: true, src: "**", cwd: 'bower_components', dest: "dist/libs"}
        ]
    },
    angular: {
        files: [
            {expand: true, src: "**", cwd: 'bower_components/bootstrap/fonts',         dest: "dist/fonts"},
            {expand: true, src: "**", cwd: 'bower_components/font-awesome/fonts',      dest: "dist/fonts"},
            {expand: true, src: "**", cwd: 'src/data',     dest: "dist/data"},
            {expand: true, src: "**", cwd: 'src/img',     dest: "dist/img"},
            {expand: true, src: "**", cwd: 'src/js',      dest: "dist/js"},
            {expand: true, src: "**", cwd: 'src/partials',     dest: "dist/partials"},
            {expand: true, src: "**", cwd: 'src/account', dest: "dist/account"},
            {expand: true, src: "index.html", cwd: 'src',     dest: "dist"},
        ]
    }
};
