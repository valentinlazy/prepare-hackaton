module.exports = {
    style: {
        files: {
            'dist/css/app.min.css': [
                'bower_components/angular-loading-bar/build/loading-bar.min.css',
                'bower_components/bootstrap/dist/css/bootstrap.css',
                'bower_components/bootstrap/dist/css/bootstrap-theme.css',
                'bower_components/animate.css/animate.css',
                'src/css/material-icons.css',
                'src/css/videogular.css',
                'src/css/app.css'
            ],
          'dist/css/creative.min.css': [
            'src/css/bootstrap.css',
            'src/css/magnific-popup.css',
            'bower_components/font-awesome/css/font-awesome.css',
            'src/css/creative.css'
          ]
        },
        options: {
            compress: true
        }
    }
};
